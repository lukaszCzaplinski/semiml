
open Compiler

let register_analyses () =
  Analysis.NuL.FreeVars.register ();
  ()

let register_transformations () =
  Transform.CPS.ScopeCheck.register ();
  Transform.MiniML.ToLambda.register ();
  Transform.MiniML.TypeCheck.Main.register ();
  Transform.NuL.PartialEval.register ();
  Transform.NuL.ScopeCheck.register ();
  Transform.NuL.UniqueTags.register ();
  Transform.NuL.UniqueVars.register ();
  Transform.NuL.UnusedLet.register ();
  ()

let register_contract_checkers () =
  ContractCheck.NuL.RightScopes.register ();
  ContractCheck.NuL.UniqueTags.register ();
  ContractCheck.NuL.UniqueVars.register ();
  ()
 
let register_evaluators () =
  register_evaluator
    ~lang: Lang_CPS
    ~name: "CPS:eval"
    Lang.CPS.Eval.eval_program;
  register_evaluator
    ~lang: Lang_Lambda
    ~name: "Lambda:eval"
    Lang.Lambda.Eval.eval_program;
  register_evaluator
    ~lang: Lang_NuL
    ~name: "NuL:eval"
    ~require: [ Lang.NuL.Contracts.right_scopes ]
    Lang.NuL.Eval.eval_program;
  ()

let register_parsers () =
  register_parser
    ~lang: Lang_RawCPS
    ~name: "RawCPS:parser"
    ~extension: "cps"
    Lang.RawCPS.Parser.parser_func;
  register_parser
    ~lang: Lang_RawMiniML
    ~name: "RawMiniML:parser"
    ~extension: "mini"
    Lang.RawMiniML.Parser.parser_func;
  register_parser
    ~lang: Lang_RawNuL
    ~name: "RawNuL:parser"
    ~extension: "nul"
    Lang.RawNuL.Parser.parser_func;
  ()

let register_printers () =
  register_pretty_printer
    ~lang: Lang_CPS
    ~name: "CPS:pretty"
    Lang.CPS.Pretty.pretty_program;
  register_pretty_printer
    ~lang: Lang_Lambda
    ~name: "Lambda:pretty"
    Lang.Lambda.Pretty.pretty_program;
  register_pretty_printer
    ~lang: Lang_NuL
    ~name: "NuL:pretty"
    Lang.NuL.Pretty.pretty_program;
  ()

let init () =
  register_analyses ();
  register_contract_checkers ();
  register_evaluators ();
  register_parsers ();
  register_printers ();
  register_transformations ();
  ()
