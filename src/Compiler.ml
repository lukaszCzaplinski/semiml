include CompilerDB.Types

let register_analysis 
    ~lang 
    ~name 
    ?(require=[]) 
    ?(contracts=[])
    ?(contract_rules=[])
    analyse =
  let analysis_obj = CompilerDB.Analysis.Analysis(lang,
    { CompilerDB.Analysis.analyse        = analyse
    ; CompilerDB.Analysis.name           = name
    ; CompilerDB.Analysis.require        = require
    ; CompilerDB.Analysis.contract_rules = 
        ([], Contract.Add contracts) :: contract_rules
    })
  in
  CompilerDB.AnalysisDB.register_analysis analysis_obj

let register_contract_checker ~lang ~contract ~name checker =
  let checker_obj = CompilerDB.ContractChecker.ContractChecker(lang,
    { CompilerDB.ContractChecker.checker  = checker
    ; CompilerDB.ContractChecker.contract = contract
    ; CompilerDB.ContractChecker.name     = name
    })
  in
  CompilerDB.ContractCheckerDB.register_contract_checker checker_obj

let register_evaluator ~lang ~name ?(require=[]) eval =
  let eval_obj = CompilerDB.Evaluator.Evaluator(lang,
    { CompilerDB.Evaluator.eval    = eval
    ; CompilerDB.Evaluator.name    = name
    ; CompilerDB.Evaluator.require = require
    })
  in
  CompilerDB.EvaluatorDB.register_evaluator eval_obj

let register_parser ~lang ~name ?extension ?(contracts=[]) parser_func =
  let parser_obj = CompilerDB.Parser.Parser(lang,
    { CompilerDB.Parser.parser_func = parser_func
    ; CompilerDB.Parser.name        = name
    ; CompilerDB.Parser.extension   = extension
    ; CompilerDB.Parser.contracts   = Contract.Set.of_list contracts
    })
  in
  CompilerDB.ParserDB.register_parser parser_obj

let register_pretty_printer ~lang ~name ?(require=[]) pretty =
  let pretty_obj = CompilerDB.Pretty.Pretty(lang,
    { CompilerDB.Pretty.pretty  = pretty
    ; CompilerDB.Pretty.name    = name
    ; CompilerDB.Pretty.require = require
    })
  in
  CompilerDB.PrettyDB.register_pretty_printer pretty_obj

let register_transformation ~source ~target 
    ~name
    ?(require=[])
    ?(contracts=[]) 
    ?(contract_rules=[]) 
    transform =
  let transform_obj = CompilerDB.Transform.Transform(source, target,
    { CompilerDB.Transform.transform      = transform
    ; CompilerDB.Transform.name           = name
    ; CompilerDB.Transform.require        = require
    ; CompilerDB.Transform.contract_rules = 
        ([], Contract.Set contracts) :: contract_rules
    }) in
  CompilerDB.TransformDB.register_transformation transform_obj

