
module Variable = struct
  type t =
    { name : string
    ; id   : int64
    }

  let compare x y = Int64.compare x.id y.id
end

include Variable

let equal x y = x == y

let name x = x.name

let next_fresh_id = ref 0L
let fresh_id () =
  let r = !next_fresh_id in
  next_fresh_id := Int64.succ r;
  r

let create ?(name="x") () =
  { name = name
  ; id = fresh_id ()
  }

let copy x =
  { x with id = fresh_id () }

module Map = Map.Make(Variable)
module Set = Set.Make(Variable)
