
type t

val compare : t -> t -> int
val equal   : t -> t -> bool

val name : t -> string

val create : ?name: string -> unit -> t
val copy   : t -> t

module Map : Map.S with type key = t
module Set : Set.S with type elt = t
