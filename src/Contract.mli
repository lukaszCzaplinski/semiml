
type t

val compare : t -> t -> int
val equal   : t -> t -> bool

val create : string -> t

val name : t -> string

type action =
| Add    of t list
| Set    of t list
| Remove of t list

type rule = t list * action

val saves_contract : t -> rule

module Set : Set.S with type elt = t

val apply_rules : rule list -> Set.t -> Set.t
