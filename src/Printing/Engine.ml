open DataTypes

exception Too_wide

type cstring =
| C_Empty
| C_Attr of attribute list * cstring
| C_Text of string
| C_Join of cstring * cstring

type multiline =
  { ml_lines    : cstring list
  ; ml_last     : cstring
  ; ml_last_box : box
  ; ml_atomic   : bool
  }

type state =
  { st_max_width  : int
  ; st_max_indent : int
  ; st_indent     : int
  ; st_pos        : int
  ; st_last_ws    : bool
  ; st_new_line   : bool
  }

type 'a result =
  { r_result     : 'a
  ; r_pos        : int
  ; r_last_ws    : bool
  ; r_break_line : bool
  }

let join_multiline ml1 ml2 =
  { ml2 with
    ml_lines = ml2.ml_lines @ (ml1.ml_last :: ml1.ml_lines)
  }

let make_prefix n ml1 ml2 =
  { ml_lines    = ml2.ml_lines @ ml1.ml_lines
  ; ml_last     = ml2.ml_last
  ; ml_last_box =
    begin
      if ml2.ml_lines = [] then
        B_Prefix(ml1.ml_last_box, ml2.ml_last_box)
      else
        B_Indent(n, ml2.ml_last_box)
    end
  ; ml_atomic   = ml2.ml_atomic
  }

let make_suffix ml1 ml2 =
  { ml_lines    = ml2.ml_lines @ ml1.ml_lines
  ; ml_last     = ml2.ml_last
  ; ml_last_box =
    begin
      if ml2.ml_lines = [] then
        B_Suffix(ml1.ml_last_box, ml2.ml_last_box)
      else 
        ml2.ml_last_box
    end
  ; ml_atomic   = ml2.ml_atomic
  }

let update_state res state =
  { state with 
    st_pos      = res.r_pos 
  ; st_last_ws  = res.r_last_ws
  ; st_new_line = false
  }

let reset_state state =
  { state with
    st_pos      = min state.st_indent state.st_max_indent
  ; st_last_ws  = true
  ; st_new_line = true
  }

let update_last_box res func =
  { res with
    r_result = 
      { res.r_result with ml_last_box = func res.r_result.ml_last_box }
  }

let is_whitespace ch =
  match ch with
  | '\n' | '\r' | '\t' | ' ' -> true
  | _ -> false

let rec pretty_attr_text state attrs str =
  match attrs with
  | [] ->
    { r_result  = C_Text str
    ; r_pos     = state.st_pos + String.length str
    ; r_last_ws =
      begin
        let l = String.length str in
        if l = 0 then state.st_last_ws
        else is_whitespace str.[l-1]
      end
    ; r_break_line = false
    }
  | _ ->
    let res = pretty_attr_text state [] str in
    { res with r_result = C_Attr(attrs, res.r_result)
    }

let rec pretty_single_line state box =
  match box with
  | B_Text(attrs, str) ->
    let res = pretty_attr_text state attrs str in
    if res.r_pos > state.st_max_width then raise Too_wide
    else res
  | B_Prefix(box1, box2) | B_Suffix(box1, box2) ->
    let res1 = pretty_single_line state box1 in
    if res1.r_break_line then raise Too_wide;
    let res2 = pretty_single_line (update_state res1 state) box2 in
    { res2 with r_result = C_Join(res1.r_result, res2.r_result) }
  | B_Indent(n, box) ->
    if state.st_new_line then begin
      let state = { state with st_pos = state.st_pos + n } in
      let res = pretty_single_line state box in
      { res with r_result = C_Join(C_Text (String.make n ' '), res.r_result) }
    end else
      pretty_single_line state box
  | B_WhiteSep box ->
    if state.st_last_ws then pretty_single_line state box
    else begin
      let state = 
        { state with 
          st_pos     = state.st_pos + 1
        ; st_last_ws = true
        }
      in
      let res = pretty_single_line state box in
      { res with r_result = C_Join(C_Text " ", res.r_result) }
    end
  | B_BreakLine box ->
    let res = pretty_single_line state box in
    { res with r_break_line = true }
  | B_Box boxes -> pretty_single_line_box state boxes

and pretty_single_line_box state boxes =
  match boxes with
  | [] ->
    if state.st_pos > state.st_max_width then raise Too_wide;
    { r_result     = C_Empty
    ; r_pos        = state.st_pos
    ; r_last_ws    = state.st_last_ws
    ; r_break_line = false
    }
  | box :: boxes ->
    let res1 = pretty_single_line state box in
    if res1.r_break_line then raise Too_wide;
    let res2 = pretty_single_line_box (update_state res1 state) boxes in
    { res2 with r_result = C_Join(res1.r_result, res2.r_result) }

let rec pretty_multiline prefix state box =
  match box with
  | B_Text(attrs, str) ->
    let res = pretty_attr_text state attrs str in
    { r_result     =
      { ml_lines    = []
      ; ml_last     = C_Join(prefix, res.r_result)
      ; ml_last_box = box
      ; ml_atomic   = true
      }
    ; r_pos        = res.r_pos
    ; r_last_ws    = res.r_last_ws
    ; r_break_line = false
    }
  | B_Prefix(box1, box2) ->
    let res1 = pretty_box prefix state box1 in
    let n = max 0 (res1.r_pos - state.st_indent) in
    if res1.r_break_line then begin
      let state = reset_state { state with st_indent = res1.r_pos } in
      let indent_str = String.make state.st_pos ' ' in
      let res2 =
        update_last_box
          (pretty_box (C_Text indent_str) state box2)
          (fun box -> B_Indent(n, box)) in
      { res2 with
        r_result = join_multiline res1.r_result res2.r_result
      }
    end else begin
      let state = update_state res1 state in
      let state = { state with st_indent = res1.r_pos } in
      let res2 = pretty_multiline res1.r_result.ml_last state box2 in
      { res2 with
        r_result = make_prefix n res1.r_result res2.r_result
      }
    end
  | B_Suffix(box1, box2) ->
    let res1 = pretty_multiline prefix state box1 in
    if res1.r_break_line then begin
      let state = reset_state state in
      let indent_str = String.make state.st_pos ' ' in
      let res2 = pretty_box (C_Text indent_str) state box2 in
      { res2 with
        r_result = join_multiline res1.r_result res2.r_result
      }
    end else if res1.r_result.ml_atomic then begin
      let state = update_state res1 state in
      let res2 = pretty_box res1.r_result.ml_last state box2 in
      { res2 with
        r_result = make_suffix res1.r_result res2.r_result
      }
    end else begin
      try
        let state = update_state res1 state in
        let res2 = pretty_single_line state box in
        { res2 with
          r_result =
            { res1.r_result with
              ml_last     = res2.r_result
            ; ml_last_box = B_Suffix(res1.r_result.ml_last_box, box2)
            }
        }
      with
      | Too_wide ->
        let state = reset_state state in
        let indent_str = String.make state.st_pos ' ' in
        let res2 =
          pretty_multiline (C_Text indent_str) state 
            (B_Suffix(res1.r_result.ml_last_box, box2))
        in
        { res2 with
          r_result =
            { res2.r_result with
              ml_lines = res2.r_result.ml_lines @ res1.r_result.ml_lines
            }
        }
    end
  | B_Indent(n, box) ->
    if state.st_new_line then begin
      let state =
        { state with
          st_indent = state.st_indent + n
        ; st_pos    = state.st_pos + n
        }
      in
      update_last_box
        (pretty_multiline (C_Join(prefix, C_Text(String.make n ' '))) state box)
        (fun box -> B_Indent(n, box))
    end else begin
      let state = { state with st_indent = state.st_indent + n } in
      update_last_box
        (pretty_multiline prefix state box)
        (fun box -> B_Indent(n, box))
    end
  | B_WhiteSep box ->
    if state.st_last_ws then pretty_multiline prefix state box
    else begin
      let state = 
        { state with 
          st_pos     = state.st_pos + 1
        ; st_last_ws = true
        }
      in
      update_last_box
        (pretty_multiline (C_Join(prefix, C_Text " ")) state box)
        (fun box -> B_WhiteSep box)
    end
  | B_BreakLine box ->
    let res = 
      update_last_box
        (pretty_multiline prefix state box)
        (fun box -> B_BreakLine box)
    in
    { res with r_break_line = true }
  | B_Box boxes -> pretty_multiline_box prefix state boxes

and pretty_multiline_box prefix state boxes =
  match boxes with
  | [] ->
    { r_result     =
      { ml_lines    = []
      ; ml_last     = prefix
      ; ml_last_box = B_Box []
      ; ml_atomic   = true
      }
    ; r_pos        = state.st_pos
    ; r_last_ws    = state.st_last_ws
    ; r_break_line = false
    }
  | [ box ] -> pretty_box prefix state box
  | box :: boxes ->
    let res1 = pretty_box prefix state box in
    let state = reset_state state in
    let indent_str = String.make state.st_pos ' ' in
    let res2 = pretty_multiline_box (C_Text indent_str) state boxes in
    { res2 with
      r_result = join_multiline res1.r_result res2.r_result
    }

and pretty_box prefix state box =
  match box with
  | B_Prefix(box1, box2) ->
    let res1 = pretty_box prefix state box1 in
    let n = max 0 (res1.r_pos - state.st_indent) in
    if res1.r_break_line then begin
      let state = reset_state { state with st_indent = res1.r_pos } in
      let indent_str = String.make state.st_pos ' ' in
      let res2 =
        update_last_box
          (pretty_box (C_Text indent_str) state box2)
          (fun box -> B_Indent(n, box)) in
      { res2 with
        r_result = join_multiline res1.r_result res2.r_result
      }
    end else begin
      let state = update_state res1 state in
      let state = { state with st_indent = res1.r_pos } in
      let res2 = pretty_box res1.r_result.ml_last state box2 in
      { res2 with
        r_result = make_prefix n res1.r_result res2.r_result
      }
    end
  | B_Suffix(box1, box2) ->
    let res1 = pretty_box prefix state box1 in
    if res1.r_break_line then begin
      let state = reset_state state in
      let indent_str = String.make state.st_pos ' ' in
      let res2 = pretty_box (C_Text indent_str) state box2 in
      { res2 with
        r_result = join_multiline res1.r_result res2.r_result
      }
    end else if res1.r_result.ml_atomic then begin
      let state = update_state res1 state in
      let res2 = pretty_box res1.r_result.ml_last state box2 in
      { res2 with
        r_result = make_suffix res1.r_result res2.r_result
      }
    end else begin
      try
        let state = update_state res1 state in
        let res2 = pretty_single_line state box in
        { res2 with
          r_result =
            { res1.r_result with
              ml_last     = res2.r_result
            ; ml_last_box = B_Suffix(res1.r_result.ml_last_box, box2)
            }
        }
      with
      | Too_wide ->
        let state = reset_state state in
        let indent_str = String.make state.st_pos ' ' in
        let res2 =
          pretty_multiline (C_Text indent_str) state 
            (B_Suffix(res1.r_result.ml_last_box, box2))
        in
        { res2 with
          r_result =
            { res2.r_result with
              ml_lines = res2.r_result.ml_lines @ res1.r_result.ml_lines
            }
        }
    end
  | _ ->
    begin try
      let res = pretty_single_line state box in
      { r_result     =
        { ml_lines    = []
        ; ml_last     = C_Join(prefix, res.r_result)
        ; ml_last_box = box
        ; ml_atomic   = false
        }
      ; r_pos        = res.r_pos
      ; r_last_ws    = res.r_last_ws
      ; r_break_line = res.r_break_line
      }
    with
    | Too_wide ->
      pretty_multiline prefix state box
    end

(* ========================================================================= *)

let rec print_cstring use_colors chan cs =
  match cs with
  | C_Empty -> ()
  | C_Attr(attrs, cs) ->
    if use_colors then ColorScheme.use_attributes chan attrs;
    print_cstring false chan cs;
    if use_colors then ColorScheme.default_attributes chan
  | C_Text str    -> output_string chan str
  | C_Join(cs1, cs2) ->
    print_cstring use_colors chan cs1;
    print_cstring use_colors chan cs2

let print_to_channel chan lines =
  let use_colors = ColorScheme.use_colors chan in
  List.iter (fun cs ->
    print_cstring use_colors chan cs;
    output_char chan '\n'
  ) (List.rev (lines.ml_last :: lines.ml_lines))

let default_state =
  { st_max_width  = 80
  ; st_max_indent = 60
  ; st_indent     = 0
  ; st_pos        = 0
  ; st_last_ws    = true
  ; st_new_line   = true
  }

let print_stdout box =
  let res = pretty_box C_Empty default_state box in
  print_to_channel stdout res.r_result

let print_stderr box =
  let res = pretty_box C_Empty default_state box in
  print_to_channel stderr res.r_result
