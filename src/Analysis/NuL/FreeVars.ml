
open Lang.NuL.Ast

let rec analyse_expr expr =
  let free_vars =
    match expr.e_kind with
    | Succ  -> Common.Var.Set.empty
    | Var x -> Common.Var.Set.singleton x
    | Num _ -> Common.Var.Set.empty
    | Abs(x, body) ->
      Common.Var.Set.remove x (analyse_expr body)
    | App(e1, e2) ->
      Common.Var.Set.union (analyse_expr e1) (analyse_expr e2)
    | Case(e, case_fun, case_zero, case_succ) ->
      List.fold_left Common.Var.Set.union Common.Var.Set.empty
      [ analyse_expr e
      ; Common.Var.Set.remove (fst case_fun) (analyse_expr (snd case_fun))
      ; analyse_expr case_zero
      ; Common.Var.Set.remove (fst case_succ) (analyse_expr (snd case_succ))
      ]
  in
  MetaData.NuL.FreeVars.set expr.e_tag free_vars;
  free_vars

let analyse expr =
  let _ : Common.Var.Set.t = analyse_expr expr in ()

let contract = Contract.create "analyse:free_vars"

let register () =
  Compiler.register_analysis
    ~lang:      Compiler.Lang_NuL
    ~name:      "NuL:free_vars"
    ~require:   [ Lang.NuL.Contracts.unique_tags ]
    ~contracts: [ contract ]
    analyse
