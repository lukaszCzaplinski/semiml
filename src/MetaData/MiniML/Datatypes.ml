
type info =
  { env      : Lang.MiniML.TypeEnv.t
  ; args     : Lang.MiniML.Type.t list
  ; datatype : Lang.MiniML.Type.datatype_def
  }

let table = Hashtbl.create 32

let get tag = 
  Hashtbl.find table tag

let try_get tag =
  try Some(get tag) with
  | Not_found -> None

let set tag info =
  Hashtbl.replace table tag info
