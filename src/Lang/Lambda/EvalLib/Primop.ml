open Common.Primop
open Types

let mk_unop op =
  V_Func (fun value cont state ->
    cont (op value) state
  )

let mk_binop op =
  V_Func (fun value cont state ->
    match Value.as_record value with
    | v1 :: v2 :: _ -> cont (op v1 v2) state
    | _ -> raise Type_error
  )

let mk_int_unop op =
  mk_unop (fun v -> V_Int (op (Value.as_int v)))

let mk_int_binop op =
  mk_binop (fun v1 v2 -> V_Int (op (Value.as_int v1) (Value.as_int v2)))

let mk_real_binop op =
  mk_binop (fun v1 v2 -> V_Real (op (Value.as_real v1) (Value.as_real v2)))

let mk_value_to_bool_unop op =
  mk_unop (fun v -> if op v then V_Int 1L else V_Int 0L)

let mk_value_to_bool_binop op =
  mk_binop (fun v1 v2 -> if op v1 v2 then V_Int 1L else V_Int 0L)

let mk_int_to_bool_binop op =
  mk_int_binop (fun x y -> if op x y then 1L else 0L)

let mk_real_to_bool_binop op =
  mk_binop (fun v1 v2 ->
    if op (Value.as_real v1) (Value.as_real v2) then V_Int 1L else V_Int 0L)

let op_arith_add        = mk_int_binop Int64.add
let op_arith_sub        = mk_int_binop Int64.sub
let op_arith_neg        = mk_int_unop  Int64.neg
let op_arith_mul        = mk_int_binop Int64.mul
let op_arith_div        = mk_int_binop Int64.div
let op_arith_mod        = mk_int_binop Int64.rem
let op_arith_rshift     = 
  mk_int_binop (fun x n -> Int64.shift_right_logical x (Int64.to_int n))
let op_arith_lshift     =
  mk_int_binop (fun x n -> Int64.shift_left x (Int64.to_int n))
let op_arith_orb        = mk_int_binop Int64.logor
let op_arith_andb       = mk_int_binop Int64.logand
let op_arith_xorb       = mk_int_binop Int64.logxor
let op_arith_notb       = mk_int_unop  Int64.lognot
let op_arith_lt         = mk_int_to_bool_binop (<)
let op_arith_le         = mk_int_to_bool_binop (<=)
let op_arith_gt         = mk_int_to_bool_binop (>)
let op_arith_ge         = mk_int_to_bool_binop (>=)
let op_arith_rangecheck = 
  mk_int_to_bool_binop (fun i j ->
    if j < 0L then
      i >= 0L || i < j
    else i >= 0L && i < j)

let op_farith_fadd = mk_real_binop Common.Real.add
let op_farith_fsub = mk_real_binop Common.Real.sub
let op_farith_fmul = mk_real_binop Common.Real.mul
let op_farith_fdiv = mk_real_binop Common.Real.div
let op_farith_feq  = mk_real_to_bool_binop Common.Real.equal
let op_farith_fneq = mk_real_to_bool_binop Common.Real.not_equal
let op_farith_flt  = mk_real_to_bool_binop Common.Real.lt
let op_farith_fle  = mk_real_to_bool_binop Common.Real.le
let op_farith_fgt  = mk_real_to_bool_binop Common.Real.gt
let op_farith_fge  = mk_real_to_bool_binop Common.Real.ge

let op_mem_deref =
  V_Func (fun value cont state ->
    let res =
      match value with
      | V_ValRef loc -> ValHeap.get state.st_val_heap loc
      | V_IntRef loc -> V_Int (IntHeap.get state.st_int_heap loc)
      | _ -> raise Type_error
    in cont res state)

let op_mem_subscript =
  V_Func (fun value cont state ->
    let res = 
      match Value.as_record value with
      | V_Array a :: V_Int n :: _ ->
        let i = Int64.to_int n in
        if i < 0 || i >= List.length a then raise Type_error
        else ValHeap.get state.st_val_heap (List.nth a i)
      | V_UArray a :: V_Int n :: _ ->
        let i = Int64.to_int n in
        if i < 0 || i >= List.length a then raise Type_error
        else V_Int(IntHeap.get state.st_int_heap (List.nth a i))
      | V_Record a :: V_Int n :: _ ->
        let i = Int64.to_int n in
        if i < 0 || i >= List.length a then raise Type_error
        else List.nth a i
      | _ -> raise Type_error
    in cont res state)

let op_mem_ordof =
  V_Func (fun value cont state ->
    let res = 
      match Value.as_record value with
      | V_String a :: V_Int n :: _ ->
        let i = Int64.to_int n in
        if i < 0 || i >= String.length a then raise Type_error
        else V_Int (Int64.of_int (Char.code (a.[i])))
      | V_ByteArray a :: V_Int n :: _ ->
        let i = Int64.to_int n in
        if i < 0 || i >= List.length a then raise Type_error
        else V_Int(IntHeap.get state.st_int_heap (List.nth a i))
      | _ -> raise Type_error
    in cont res state)

let op_mem_assign =
  V_Func (fun value cont state ->
    let state =
      match Value.as_record value with
      | V_ValRef loc :: v :: _ ->
        { state with 
          st_val_heap = ValHeap.set state.st_val_heap loc v
        }
      | V_IntRef loc :: V_Int v :: _ ->
        { state with
          st_int_heap = IntHeap.set state.st_int_heap loc v
        }
      | _ -> raise Type_error
    in cont (V_Int 0L) state)

let op_mem_unboxedassign =
  V_Func (fun value cont state ->
    let state =
      match Value.as_record value with
      | V_ValRef loc :: (V_Int _ as v) :: _ ->
        { state with 
          st_val_heap = ValHeap.set state.st_val_heap loc v
        }
      | V_IntRef loc :: V_Int v :: _ ->
        { state with
          st_int_heap = IntHeap.set state.st_int_heap loc v
        }
      | _ -> raise Type_error
    in cont (V_Int 0L) state)

let op_mem_update =
  V_Func (fun value cont state ->
    let state =
      match Value.as_record value with
      | V_Array a :: V_Int n :: v :: _ ->
        let i = Int64.to_int n in
        if i < 0 || i >= List.length a then raise Type_error
        else 
          { state with
            st_val_heap = ValHeap.set state.st_val_heap (List.nth a i) v
          }
      | V_UArray a :: V_Int n :: V_Int v :: _ ->
        let i = Int64.to_int n in
        if i < 0 || i >= List.length a then raise Type_error
        else 
          { state with 
            st_int_heap = IntHeap.set state.st_int_heap (List.nth a i) v
          }
      | _ -> raise Type_error
    in cont (V_Int 0L) state)

let op_mem_unboxedupdate =
  V_Func (fun value cont state ->
    let state =
      match Value.as_record value with
      | V_Array a :: V_Int n :: (V_Int _ as v) :: _ ->
        let i = Int64.to_int n in
        if i < 0 || i >= List.length a then raise Type_error
        else 
          { state with
            st_val_heap = ValHeap.set state.st_val_heap (List.nth a i) v
          }
      | V_UArray a :: V_Int n :: V_Int v :: _ ->
        let i = Int64.to_int n in
        if i < 0 || i >= List.length a then raise Type_error
        else 
          { state with 
            st_int_heap = IntHeap.set state.st_int_heap (List.nth a i) v
          }
      | _ -> raise Type_error
    in cont (V_Int 0L) state)

let op_mem_store =
  V_Func (fun value cont state ->
    let state =
      match Value.as_record value with
      | V_ByteArray a :: V_Int n :: V_Int v :: _ ->
        let i = Int64.to_int n in
        if i < 0 || i >= List.length a then raise Type_error
        else if v < 0L || v >= 256L then raise Type_error
        else
          { state with 
            st_int_heap = IntHeap.set state.st_int_heap (List.nth a i) v
          }
      | _ -> raise Type_error
    in cont (V_Int 0L) state)

let op_mem_makeref =
  V_Func (fun value cont state ->
    let (heap, loc) = ValHeap.mk_ref state.st_val_heap value in
    let state = { state with st_val_heap = heap } in
    cont (V_ValRef loc) state)

let op_mem_makerefunboxed =
  V_Func (fun value cont state ->
    let v = Value.as_int value in
    let (heap, loc) = IntHeap.mk_ref state.st_int_heap v in
    let state = { state with st_int_heap = heap } in
    cont (V_IntRef loc) state)

let op_repr_boxed   = mk_value_to_bool_unop Value.is_boxed
let op_repr_alength =
  mk_unop (fun value ->
    match value with
    | V_Array  a -> V_Int (Int64.of_int (List.length a))
    | V_UArray a -> V_Int (Int64.of_int (List.length a))
    | _ -> raise Type_error)
let op_repr_slength =
  mk_unop (fun value ->
    match value with
    | V_String    a -> V_Int (Int64.of_int (String.length a))
    | V_ByteArray a -> V_Int (Int64.of_int (List.length a))
    | _ -> raise Type_error)

let op_repr_phys_eq  = mk_value_to_bool_binop Value.phys_eq
let op_repr_phys_neq = mk_value_to_bool_binop 
  (fun v1 v2 -> not (Value.phys_eq v1 v2))

let op_control_callcc =
  V_Func (fun value cont state ->
    let handler = state.st_handler in
    let f = Value.as_func value in
    let cont' value state =
      cont value { state with st_handler = handler }
    in f (V_Cont cont') cont state
  )

let op_control_throw =
  V_Func (fun value cont state ->
    let cont' = Value.as_cont value in
    cont (V_Func (fun v _ state -> cont' v state)) state
  )

let eval_prim_arith prim =
  match prim with
  | Arith.Add        -> op_arith_add
  | Arith.Sub        -> op_arith_sub
  | Arith.Neg        -> op_arith_neg
  | Arith.Mul        -> op_arith_mul
  | Arith.Div        -> op_arith_div
  | Arith.Mod        -> op_arith_mod
  | Arith.RShift     -> op_arith_rshift
  | Arith.LShift     -> op_arith_lshift
  | Arith.Orb        -> op_arith_orb
  | Arith.Andb       -> op_arith_andb
  | Arith.Xorb       -> op_arith_xorb
  | Arith.Notb       -> op_arith_notb
  | Arith.Lt         -> op_arith_lt
  | Arith.Le         -> op_arith_le
  | Arith.Gt         -> op_arith_gt
  | Arith.Ge         -> op_arith_ge
  | Arith.RangeCheck -> op_arith_rangecheck

let eval_prim_farith prim =
  match prim with
  | FArith.FAdd -> op_farith_fadd
  | FArith.FSub -> op_farith_fsub
  | FArith.FMul -> op_farith_fmul
  | FArith.FDiv -> op_farith_fdiv
  | FArith.FEq  -> op_farith_feq
  | FArith.FNeq -> op_farith_fneq
  | FArith.FLt  -> op_farith_flt
  | FArith.FLe  -> op_farith_fle
  | FArith.FGt  -> op_farith_fgt
  | FArith.FGe  -> op_farith_fge

let eval_prim_mem prim =
  match prim with
  | Mem.Deref          -> op_mem_deref
  | Mem.Subscript      -> op_mem_subscript
  | Mem.OrdOf          -> op_mem_ordof
  | Mem.Assign         -> op_mem_assign
  | Mem.UnboxedAssign  -> op_mem_unboxedassign
  | Mem.Update         -> op_mem_update
  | Mem.UnboxedUpdate  -> op_mem_unboxedupdate
  | Mem.Store          -> op_mem_store
  | Mem.MakeRef        -> op_mem_makeref
  | Mem.MakeRefUnboxed -> op_mem_makerefunboxed

let eval_prim_repr prim =
  match prim with
  | Repr.Boxed   -> op_repr_boxed
  | Repr.ALength -> op_repr_alength
  | Repr.SLength -> op_repr_slength
  | Repr.PhysEq  -> op_repr_phys_eq
  | Repr.PhysNEq -> op_repr_phys_neq

let eval_prim_control prim =
  match prim with
  | Control.CallCC -> op_control_callcc
  | Control.Throw  -> op_control_throw
