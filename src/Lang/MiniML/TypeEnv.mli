
type typedef =
| Abstract of int
| TypeDef  of Type.tvar list * Type.t
| DataType of Type.tvar list * (Type.cname * Type.t option) list

type t

type scope = Type.TVarScope.t * Type.TConScope.t

val arity : t -> Type.tcon -> int

val empty : t

val add_tvar : t -> Type.tvar -> t * Type.tvar
(** in [add_fresh_tvar env x] variable [x] must be fresh *)
val add_fresh_tvar : t -> Type.tvar -> t

val add_tcon : t -> Type.tcon -> typedef -> t * Type.tcon
(** in [add_fresh_tcon env x def] variable [x] must be fresh *)
val add_fresh_tcon : t -> Type.tcon -> typedef -> t
val get_tcon : t -> Type.tcon -> typedef

val upgrade_to_datatype :
  t -> Type.tvar list -> Type.tcon -> (Type.cname * Type.t option) list -> t

val scope : t -> scope
