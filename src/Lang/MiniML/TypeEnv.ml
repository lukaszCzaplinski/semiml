
type typedef =
| Abstract of int
| TypeDef  of Type.tvar list * Type.t
| DataType of Type.tvar list * (Type.cname * Type.t option) list

type t =
  { tvar_env : Type.TVar.Set.t
  ; tcon_env : typedef Type.TCon.Map.t
  }

type scope = Type.TVarScope.t * Type.TConScope.t

let arity env x =
  match Type.TCon.Map.find x env.tcon_env with
  | Abstract n -> n
  | TypeDef(args, _) | DataType(args, _) -> List.length args

let empty =
  { tvar_env = Type.TVar.Set.empty
  ; tcon_env =
    List.fold_left (fun env (x, n) ->
      Type.TCon.Map.add x (Abstract n) env
    ) Type.TCon.Map.empty Type.predefined_types
  }

let add_fresh_tvar env x =
  { env with
    tvar_env = Type.TVar.Set.add x env.tvar_env
  }

let add_tvar env x =
  let x =
    if Type.TVar.Set.mem x env.tvar_env then Type.TVar.copy x
    else x
  in (add_fresh_tvar env x, x)

let add_fresh_tcon env x def =
  { env with
    tcon_env = Type.TCon.Map.add x def env.tcon_env
  }

let add_tcon env x def =
  let x =
    if Type.TCon.Map.mem x env.tcon_env then Type.TCon.copy x
    else x
  in
  (add_fresh_tcon env x def, x)

let get_tcon env x =
  Type.TCon.Map.find x env.tcon_env

let upgrade_to_datatype env args x ctors =
  assert (Type.TCon.Map.mem x env.tcon_env
    && Type.TCon.Map.find x env.tcon_env = Abstract (List.length args));
  { env with
    tcon_env = Type.TCon.Map.add x (DataType(args, ctors)) env.tcon_env
  }

let scope env =
  let tvar_scope = Type.TVarScope.create Type.TVar.name in
  let tvar_scope =
    Type.TVar.Set.fold (fun x scope -> 
      fst (Type.TVarScope.extend_hide scope x)
    ) env.tvar_env tvar_scope in
  let tcon_scope = Type.TConScope.create Type.TCon.name in
  let tcon_scope =
    Type.TCon.Map.fold (fun x _ scope ->
      fst (Type.TConScope.extend_hide scope x)
    ) env.tcon_env tcon_scope in
  (tvar_scope, tcon_scope)
