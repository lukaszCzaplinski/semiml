
(** Type variables *)
type tvar =
| TVar of Common.Var.t

(** Type constructors (names of types) *)
type tcon =
| TCon of Common.Var.t

(** Name of a datatype constructor *)
type cname =
| CName of Common.Var.t

type t =
| Var       of tvar
| TyCon     of t list * tcon
| Arrow     of t * t
| Record    of t list
| TypeDef   of tvar list * tcon * t * t
| DataType  of datatype_def list * t
| ForallVar of tvar * t
| Forall    of int * tcon * t
| Exists    of int * tcon * t

and datatype_def =
  { dt_args  : tvar list
  ; dt_name  : tcon
  ; dt_ctors : (cname * t option) list
  }

module TVar : sig
  type t = tvar
  val compare : t -> t -> int
  val equal   : t -> t -> bool
  val name    : t -> string
  val create  : ?name: string -> unit -> t
  val copy    : t -> t
  module Map : Map.S with type key = t
  module Set : Set.S with type elt = t
end

module TCon : sig
  type t = tcon
  val compare : t -> t -> int
  val equal   : t -> t -> bool
  val name    : t -> string
  val create  : ?name: string -> unit -> t
  val copy    : t -> t
  module Map : Map.S with type key = t
  module Set : Set.S with type elt = t
end

module CName : sig
  type t = cname
  val compare : t -> t -> int
  val equal   : t -> t -> bool
  val name    : t -> string
  val create  : ?name: string -> unit -> t
  val copy    : t -> t
  module Map : Map.S with type key = t
  module Set : Set.S with type elt = t
end

module TVarScope : Printing.Scope.S with type key = tvar
module TConScope : Printing.Scope.S with type key = tcon

val t_int      : tcon
val t_real     : tcon
val t_string   : tcon
val t_exn      : tcon
val t_exn_ctor : tcon
val predefined_types : (tcon * int) list

val contains_tcon : tcon -> t -> bool
