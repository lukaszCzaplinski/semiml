
(** Type variables *)
type tvar =
| TVar of Common.Var.t

(** Type constructors (names of types) *)
type tcon =
| TCon of Common.Var.t

(** Name of a datatype constructor *)
type cname =
| CName of Common.Var.t

type t =
| Var       of tvar
| TyCon     of t list * tcon
| Arrow     of t * t
| Record    of t list
| TypeDef   of tvar list * tcon * t * t
| DataType  of datatype_def list * t
| ForallVar of tvar * t
| Forall    of int * tcon * t
| Exists    of int * tcon * t

and datatype_def =
  { dt_args  : tvar list
  ; dt_name  : tcon
  ; dt_ctors : (cname * t option) list
  }

module TVarCore = struct
  type t = tvar
  let compare (TVar x) (TVar y) = Common.Var.compare x y
  let equal (TVar x) (TVar y) = Common.Var.equal x y
  let name (TVar x) = Common.Var.name x
  let create ?name () = TVar(Common.Var.create ?name ())
  let copy (TVar x) = TVar(Common.Var.copy x)
end

module TVar = struct
  include TVarCore
  module Map = Map.Make(TVarCore)
  module Set = Set.Make(TVarCore)
end

module TConCore = struct
  type t = tcon
  let compare (TCon x) (TCon y) = Common.Var.compare x y
  let equal (TCon x) (TCon y) = Common.Var.equal x y
  let name (TCon x) = Common.Var.name x
  let create ?name () = TCon(Common.Var.create ?name ())
  let copy (TCon x) = TCon(Common.Var.copy x)
end

module TCon = struct
  include TConCore
  module Map = Map.Make(TConCore)
  module Set = Set.Make(TConCore)
end

module CNameCore = struct
  type t = cname
  let compare (CName x) (CName y) = Common.Var.compare x y
  let equal (CName x) (CName y) = Common.Var.equal x y
  let name (CName x) = Common.Var.name x
  let create ?name () = CName(Common.Var.create ?name ())
  let copy (CName x) = CName(Common.Var.copy x)
end

module CName = struct
  include CNameCore
  module Map = Map.Make(CNameCore)
  module Set = Set.Make(CNameCore)
end

module TVarScope = Printing.Scope.Make(TVar)
module TConScope = Printing.Scope.Make(TCon)

let t_int = TCon(Common.Var.create ~name: "int" ())
let t_real = TCon(Common.Var.create ~name: "real" ())
let t_string = TCon(Common.Var.create ~name: "string" ())
let t_exn = TCon(Common.Var.create ~name: "exn" ())
let t_exn_ctor = TCon(Common.Var.create ~name: "exn_ctor" ())
let predefined_types =
  [ t_int,      0
  ; t_real,     0
  ; t_string,   0
  ; t_exn,      0
  ; t_exn_ctor, 1
  ]

let rec contains_tcon x tp =
  match tp with
  | Var _ -> false
  | TyCon(args, y) -> TCon.equal x y || List.exists (contains_tcon x) args
  | Arrow(tp1, tp2) -> contains_tcon x tp1 || contains_tcon x tp2
  | Record tps -> List.exists (contains_tcon x) tps
  | TypeDef(_, y, body, tp) ->
    contains_tcon x body ||
    (not (TCon.equal x y) && contains_tcon x tp)
  | DataType(defs, tp) ->
    List.for_all (fun dt -> not (TCon.equal x dt.dt_name)) defs &&
    begin
      contains_tcon x tp ||
      List.exists (fun dt ->
        List.exists (fun (_, ct) ->
          match ct with
          | None    -> false
          | Some tp -> contains_tcon x tp
        ) dt.dt_ctors
      ) defs
    end
  | ForallVar(_, tp) -> contains_tcon x tp
  | Forall(_, y, tp) | Exists(_, y, tp) ->
    not (TCon.equal x y) && contains_tcon x tp
