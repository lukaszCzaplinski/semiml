
type var  = Common.Var.t
type tvar = Type.tvar

type expr =
  { e_tag  : Common.Tag.t
  ; e_kind : expr_kind
  }
and expr_kind =
| Var        of var
| Fn         of var * Type.t * expr
| Fix        of fix_def list * expr
| App        of expr * expr
| Int        of int64
| Real       of Common.Real.t
| String     of string
| Case       of case_expr
| Con        of Type.cname * Type.t list * expr option
| CaseInt    of expr * (int64 * expr) list * default_case
| CaseExn    of expr * (expr * var option * expr) list * default_case
| Exn        of expr
| ExnCtor    of expr * Type.t
| ConExn     of expr * expr
| ExnName    of expr
| Record     of expr list
| Select     of int * expr
| Raise      of expr * Type.t
| Handle     of expr * expr
| Prim       of Primop.t
| TypeDef    of tvar list * Type.tcon * Type.t * expr
| DataType   of Type.datatype_def list * expr
| TypeVarFn  of tvar * expr
| TypeVarApp of expr * Type.t
| TypeFn     of int * Type.tcon * expr
| TypeApp    of expr * tvar list * Type.t
| Pack       of tvar list * Type.tcon * Type.t * expr * Type.t
| Unpack     of int * Type.tcon * var * expr * expr

and fix_def =
  { fxd_var  : var
  ; fxd_type : Type.t
  ; fxd_body : expr
  }

and case_expr =
  { ce_expr          : expr
  ; ce_cases         : (Type.cname * var option * expr) list
  ; ce_default_case  : default_case option
  }

and default_case = var * expr

module Expr : sig
  type t = expr

  val is_function : t -> bool
  val is_value    : t -> bool
end
