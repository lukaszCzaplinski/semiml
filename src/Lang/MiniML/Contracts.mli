
(** Contract [well_typed] states that the program is well-typed *)
val well_typed : Contract.t
