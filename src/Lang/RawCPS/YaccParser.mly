%token<string> ID STRING
%token<int64>  INT
%token<Common.Real.t> REAL
%token EOF
%token BR_OPN BR_CLS
%token ARROW2 COMMA EQ HASH
%token KW_AND KW_AS KW_CALL KW_END KW_FIX KW_LABEL KW_LET KW_OFFSET KW_PRIMOP
%token KW_RECORD KW_SWITCH

%type <Ast.expr> main
%start main

%{

let mk_expr pos kind =
  let tag = Common.Tag.fresh () in
  LangMetaData.Position.set tag pos;
  { Ast.e_tag  = tag
  ; Ast.e_kind = kind
  }

let mk_value kind =
  let tag = Common.Tag.fresh () in
  LangMetaData.Position.set tag (Common.Position.parsing_current_pos ());
  { Ast.v_tag  = tag
  ; Ast.v_kind = kind
  }

let mk_accesspath pos kind =
  let tag = Common.Tag.fresh () in
  LangMetaData.Position.set tag pos;
  { Ast.ap_tag  = tag
  ; Ast.ap_kind = kind
  }

%}

%%

main
: expr EOF { $1 }
;

/* ========================================================================= */

expr
: record_expr { $1 }
| select_expr { $1 }
| offset_expr { $1 }
| call_expr   { $1 }
| fix_expr    { $1 }
| switch_expr { $1 }
| primop_expr { $1 }
;

/* ========================================================================= */

record_expr
: record_expr_body expr { $1 $2 }
;

record_expr_body
: KW_LET ID EQ KW_RECORD value_ap_list_rev {
    let pos = Common.Position.parsing_current_pos () in
    fun cont -> mk_expr pos (Ast.Record(List.rev $5, $2, cont))
  }
;

/* ========================================================================= */

select_expr
: select_expr_body expr { $1 $2 }
;

select_expr_body
: KW_LET ID EQ HASH INT value {
    let pos = Common.Position.parsing_current_pos () in
    fun cont -> mk_expr pos (Ast.Select(Int64.to_int $5, $6, $2, cont))
  }
;

/* ========================================================================= */

offset_expr
: offset_expr_body expr { $1 $2 }
;

offset_expr_body
: KW_LET ID EQ KW_OFFSET INT value {
    let pos = Common.Position.parsing_current_pos () in
    fun cont -> mk_expr pos (Ast.Offset(Int64.to_int $5, $6, $2, cont))
  }
;

/* ========================================================================= */

call_expr
: KW_CALL value call_args {
    let pos = Common.Position.parsing_current_pos () in
    mk_expr pos (Ast.App($2, $3))
  }
;

/* ========================================================================= */

fix_expr
: fix_expr_body expr { $1 $2 }
;

fix_expr_body
: KW_FIX fix_def_and_sep_list_rev {
    let pos = Common.Position.parsing_current_pos () in
    fun cont -> mk_expr pos (Ast.Fix(List.rev $2, cont))
  }
;

fix_def_and_sep_list_rev
: fix_def { [ $1 ] }
| fix_def_and_sep_list_rev KW_AND fix_def { $3 :: $1 }
;

fix_def
: ID formal_args EQ expr {
    ($1, $2, $4)
  }
;

formal_args
: BR_OPN BR_CLS { [] }
| BR_OPN id_comma_sep_list_rev BR_CLS { List.rev $2 }
;

id_comma_sep_list_rev
: ID { [ $1 ] }
| id_comma_sep_list_rev COMMA ID { $3 :: $1 }
;

/* ========================================================================= */

switch_expr
: KW_SWITCH value clause_list_rev KW_END {
    let pos = Common.Position.parsing_current_pos () in
    mk_expr pos (Ast.Switch($2, List.rev $3))
  }
;

/* ========================================================================= */

primop_expr
: primop_expr_body expr { $1 [ $2 ]        }
| primop_expr_body clause_list_rev KW_END { $1 (List.rev $2) }
;

primop_expr_body
: KW_LET ID EQ KW_PRIMOP ID call_args {
    let pos = Common.Position.parsing_current_pos () in
    fun conts -> mk_expr pos (Ast.Primop($5, $6, [$2], conts))
  }
| KW_PRIMOP ID call_args {
    let pos = Common.Position.parsing_current_pos () in
    fun conts -> mk_expr pos (Ast.Primop($2, $3, [], conts))
  }
| KW_PRIMOP ID call_args KW_AS id_comma_sep_list_rev {
    let pos = Common.Position.parsing_current_pos () in
    fun conts -> mk_expr pos (Ast.Primop($2, $3, List.rev $5, conts))
  }
;

/* ========================================================================= */

clause_list_rev
: /* empty */            { [] }
| clause_list_rev clause { $2 :: $1 }
;

clause
: ARROW2 expr { $2 }
;

/* ========================================================================= */

value
: ID          { mk_value (Ast.Var $1)    }
| KW_LABEL ID { mk_value (Ast.Label $2)  }
| INT         { mk_value (Ast.Int $1)    }
| REAL        { mk_value (Ast.Real $1)   }
| STRING      { mk_value (Ast.String $1) }
;

/* ========================================================================= */

value_ap_list_rev
: /* empty */                { [] }
| value_ap_list_rev value_ap { $2 :: $1 }
;

value_ap
: value accesspath { ($1, $2) }
;

accesspath
: /* empty */ {
    let pos = Common.Position.parsing_current_pos () in
    mk_accesspath pos (Ast.Offp 0)
  }
| KW_OFFSET INT {
    let pos = Common.Position.parsing_current_pos () in
    mk_accesspath pos (Ast.Offp (Int64.to_int $2))
  }
| sel_accesspath_elem accesspath { $1 $2 }
;

sel_accesspath_elem
: HASH INT {
    let pos = Common.Position.parsing_current_pos () in
    fun ap -> mk_accesspath pos (Ast.Selp(Int64.to_int $2, ap))
  }
;

/* ========================================================================= */

call_args
: BR_OPN BR_CLS { [] }
| BR_OPN value_comma_sep_list_rev BR_CLS { List.rev $2 }
;

value_comma_sep_list_rev
: value { [ $1 ] }
| value_comma_sep_list_rev COMMA value { $3 :: $1 }
;
