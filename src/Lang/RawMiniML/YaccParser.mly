%token <string> ID TVAR STRING
%token <int64> INT
%token <Common.Real.t> REAL
%token EOF
%token BR_OPN BR_CLS SBR_OPN SBR_CLS CBR_OPN CBR_CLS
%token ARROW ARROW2 BAR COLON COMMA EQ HASH
%token KW_AND KW_DATATYPE KW_DEFAULT KW_END KW_EXCEPTION KW_EXISTS KW_EXNNAME
%token KW_FN KW_FORALL KW_FROM KW_HANDLE KW_IN KW_LET KW_MATCH KW_MATCHEXN
%token KW_OF KW_PACK KW_RAISE KW_REC KW_TYPE KW_UNPACK KW_WITH

%type <Ast.expr> main
%start main

%{

let mk_ident name =
  let tag = Common.Tag.fresh () in
  LangMetaData.Position.set tag (Common.Position.parsing_current_pos ());
  { Ast.id_tag  = tag
  ; Ast.id_name = name
  }

let mk_typ kind =
  let tag = Common.Tag.fresh () in
  LangMetaData.Position.set tag (Common.Position.parsing_current_pos ());
  { Ast.t_tag  = tag
  ; Ast.t_kind = kind
  }

let change_typ_pos typ =
  LangMetaData.Position.set typ.Ast.t_tag
    (Common.Position.parsing_current_pos ());
  typ

let mk_arg kind =
  let tag = Common.Tag.fresh () in
  LangMetaData.Position.set tag (Common.Position.parsing_current_pos ());
  { Ast.arg_tag  = tag
  ; Ast.arg_kind = kind
  }

let mk_expr kind =
  let tag = Common.Tag.fresh () in
  LangMetaData.Position.set tag (Common.Position.parsing_current_pos ());
  { Ast.e_tag  = tag
  ; Ast.e_kind = kind
  }

let change_expr_pos expr =
  LangMetaData.Position.set expr.Ast.e_tag
    (Common.Position.parsing_current_pos ());
  expr

%}

%%

main
: expr EOF { $1 }
;

/* ========================================================================= */

arg
: BR_OPN id id_list COLON typ BR_CLS {
    mk_arg (Ast.Arg_Annot($2 :: $3, $5))
  }
| id {
    mk_arg (Ast.Arg_Var $1)
  }
| SBR_OPN tvar_id tvar_id_list_rev SBR_CLS {
    mk_arg (Ast.Arg_TypeVar($2 :: List.rev $3))
  }
| SBR_OPN KW_TYPE typedecl_and_sep_list_rev SBR_CLS {
    mk_arg (Ast.Arg_Type(List.rev $3))
  }
;

arg_list
: arg_list_rev { List.rev $1 }
;

arg_list_rev
: /* empty */      { [] }
| arg_list_rev arg { $2 :: $1 }
;

/* ========================================================================= */

expr
: expr_stmt { $1 }
;

expr_stmt
: KW_FN arg arg_list ARROW2 expr_stmt {
    mk_expr (Ast.Fn($2 :: $3, $5))
  }
| KW_LET id EQ expr KW_IN expr_stmt {
    mk_expr (Ast.Let($2, $4, $6))
  }
| KW_LET KW_REC fix_def_and_sep_list_rev KW_IN expr_stmt {
    mk_expr (Ast.Fix(List.rev $3, $5))
  }
| KW_TYPE type_def_and_sep_list_rev KW_IN expr_stmt {
    mk_expr (Ast.TypeDef(List.rev $2, $4))
  }
| KW_DATATYPE datatype_def_and_sep_list_rev KW_IN expr_stmt {
    mk_expr (Ast.Datatype(List.rev $2, $4))
  }
| KW_PACK KW_TYPE type_def_and_sep_list_rev KW_WITH expr pack_annot_opt {
    mk_expr (Ast.Pack(List.rev $3, $5, $6))
  }
| KW_UNPACK KW_TYPE typedecl_and_sep_list_rev KW_WITH id 
      KW_FROM expr KW_IN expr_stmt {
    mk_expr (Ast.Unpack(List.rev $3, $5, $7, $9))
  }
| expr_annot { $1 }
;

expr_annot
: expr_annot COLON typ { mk_expr (Ast.Annot($1, $3)) }
| expr_raise { $1 }
;

expr_raise
: KW_RAISE expr_raise { mk_expr (Ast.Raise $2) }
| KW_EXCEPTION expr_atom { mk_expr (Ast.Exn $2) }
| KW_EXCEPTION expr_atom KW_OF typ { mk_expr (Ast.ExnCtor($2, $4)) }
| expr_handle { $1 }
;

expr_handle
: expr_handle KW_HANDLE expr_app { mk_expr (Ast.Handle($1, $3)) }
| expr_app { $1 }
;

expr_app
: expr_app expr_atom { mk_expr (Ast.App($1, $2)) }
| expr_app SBR_OPN typ SBR_CLS { mk_expr (Ast.TypeVarApp($1, $3)) }
| expr_app SBR_OPN KW_TYPE type_def_and_sep_list_rev SBR_CLS {
    mk_expr (Ast.TypeApp($1, List.rev $4))
  }
| HASH INT expr_atom { mk_expr (Ast.Select(Int64.to_int $2, $3)) }
| KW_EXNNAME expr_atom { mk_expr (Ast.ExnName $2) }
| expr_atom { $1 }
;

expr_atom
: ID     { mk_expr (Ast.Var $1) }
| BR_OPN expr BR_CLS { change_expr_pos $2 }
| INT    { mk_expr (Ast.Int $1)    }
| REAL   { mk_expr (Ast.Real $1)   }
| STRING { mk_expr (Ast.String $1) }
| KW_MATCH expr KW_WITH bar_opt 
      ctor_case_bar_sep_list_rev default_case_opt KW_END {
    mk_expr (Ast.Case($2, List.rev $5, $6))
  }
| KW_MATCH expr KW_WITH
      bar_opt int_case_bar_sep_list_rev default_case KW_END {
    mk_expr (Ast.CaseInt($2, List.rev $5, $6))
  }
| KW_MATCHEXN expr KW_WITH
      bar_opt exn_case_bar_sep_list_rev default_case KW_END {
    mk_expr (Ast.CaseExn($2, List.rev $5, $6))
  }
| CBR_OPN CBR_CLS { mk_expr (Ast.Record []) }
| CBR_OPN expr_comma_sep_list_rev CBR_CLS {
    mk_expr (Ast.Record (List.rev $2))
  }
;

/* ========================================================================= */

expr_comma_sep_list_rev
: expr { [ $1 ] }
| expr_comma_sep_list_rev COMMA expr { $3 :: $1 }
;

/* ========================================================================= */

pack_annot_opt
: KW_END    { None }
| KW_IN typ { Some $2 }
;

/* ========================================================================= */

fix_def
: id arg_list COLON typ EQ expr {
    let tag = Common.Tag.fresh () in
    LangMetaData.Position.set tag (Common.Position.parsing_current_pos ());
    { Ast.fxd_tag  = tag
    ; Ast.fxd_name = $1
    ; Ast.fxd_args = $2
    ; Ast.fxd_type = $4
    ; Ast.fxd_body = $6
    }
  }
;

fix_def_and_sep_list_rev
: fix_def { [ $1 ] }
| fix_def_and_sep_list_rev KW_AND fix_def { $3 :: $1 }
;

/* ========================================================================= */

ctor_case
: id    ARROW2 expr {
    { Ast.cc_ctor = $1
    ; Ast.cc_arg  = None
    ; Ast.cc_body = $3
    }
  }
| id id ARROW2 expr { 
    { Ast.cc_ctor = $1
    ; Ast.cc_arg  = Some $2
    ; Ast.cc_body = $4
    }
  }
;

ctor_case_bar_sep_list_rev
: ctor_case { [ $1 ] }
| ctor_case_bar_sep_list_rev BAR ctor_case { $3 :: $1 }
;

/* ========================================================================= */

int_case
: INT ARROW2 expr { ($1, $3) }
;

int_case_bar_sep_list_rev
: int_case { [ $1 ] }
| int_case_bar_sep_list_rev BAR int_case { $3 :: $1 }
;

/* ========================================================================= */

exn_case
: expr_atom    ARROW2 expr {
    { Ast.ec_exn  = $1
    ; Ast.ec_arg  = None
    ; Ast.ec_body = $3
    }
  }
| expr_atom id ARROW2 expr {
    { Ast.ec_exn  = $1
    ; Ast.ec_arg  = Some $2
    ; Ast.ec_body = $4
    }
  }
;

exn_case_bar_sep_list_rev
: exn_case { [ $1 ] }
| exn_case_bar_sep_list_rev BAR exn_case { $3 :: $1 }
;

/* ========================================================================= */

default_case
: KW_DEFAULT id ARROW2 expr { ($2, $4) }
;

default_case_opt
: /* empty */  { None    }
| default_case { Some $1 }
;

/* ========================================================================= */

formal_tp_args
: /* empty */ { [] }
| tvar_id { [ $1 ] }
| BR_OPN tvar_id_comma_sep_list_rev BR_CLS { List.rev $2 }
;

/* ========================================================================= */

type_def
: formal_tp_args id EQ typ {
    let tag = Common.Tag.fresh () in
    LangMetaData.Position.set tag (Common.Position.parsing_current_pos ());
    { Ast.tdef_tag  = tag
    ; Ast.tdef_args = $1
    ; Ast.tdef_name = $2
    ; Ast.tdef_body = $4
    }
  }
;

type_def_and_sep_list_rev
: type_def { [ $1 ] }
| type_def_and_sep_list_rev KW_AND type_def { $3 :: $1 }
;

/* ========================================================================= */

typedecl
: formal_tp_args id {
    let tag = Common.Tag.fresh () in
    LangMetaData.Position.set tag (Common.Position.parsing_current_pos ());
    { Ast.tdecl_tag  = tag
    ; Ast.tdecl_args = $1
    ; Ast.tdecl_name = $2
    }
  }
;

typedecl_and_sep_list_rev
: typedecl { [ $1 ] }
| typedecl_and_sep_list_rev KW_AND typedecl { $3 :: $1 }
;

/* ========================================================================= */

typ
: typ_stmt { $1 }
;

typ_stmt
: KW_TYPE type_def_and_sep_list_rev KW_IN typ_stmt {
    mk_typ (Ast.T_TypeDef(List.rev $2, $4))
  }
| KW_DATATYPE datatype_def_and_sep_list_rev KW_IN typ_stmt {
    mk_typ (Ast.T_Datatype(List.rev $2, $4))
  }
| KW_FORALL tvar_id tvar_id_list_rev COMMA typ_stmt {
    mk_typ (Ast.T_ForallVar($2 :: List.rev $3, $5))
  }
| KW_FORALL KW_TYPE typedecl_and_sep_list_rev COMMA typ_stmt {
    mk_typ (Ast.T_Forall(List.rev $3, $5))
  }
| KW_EXISTS KW_TYPE typedecl_and_sep_list_rev COMMA typ_stmt {
    mk_typ (Ast.T_Exists(List.rev $3, $5))
  }
| typ_atom ARROW typ_stmt { mk_typ (Ast.T_Arrow($1, $3)) }
| typ_atom { $1 }
;

typ_atom
: BR_OPN typ BR_CLS { change_typ_pos $2 }
| BR_OPN typ COMMA typ_comma_sep_list_rev BR_CLS id {
    mk_typ (Ast.T_Con($2 :: List.rev $4, $6))
  }
| typ_atom id { mk_typ (Ast.T_Con([$1], $2)) }
| id { mk_typ (Ast.T_Con([], $1)) }
| TVAR { mk_typ (Ast.T_Var $1) }
| CBR_OPN CBR_CLS { mk_typ (Ast.T_Record []) }
| CBR_OPN typ_comma_sep_list_rev CBR_CLS { 
    mk_typ (Ast.T_Record (List.rev $2))
  }
;

typ_comma_sep_list_rev
: typ { [ $1 ] }
| typ_comma_sep_list_rev COMMA typ { $3 :: $1 }
;

/* ========================================================================= */

datatype_def
: formal_tp_args id EQ bar_opt ctor_def_bar_sep_list_rev {
    { Ast.dt_args  = $1
    ; Ast.dt_name  = $2
    ; Ast.dt_ctors = List.rev $5
    }
  }
;

datatype_def_and_sep_list_rev
: datatype_def { [ $1 ] }
| datatype_def_and_sep_list_rev KW_AND datatype_def { $3 :: $1 }
;

/* ========================================================================= */

ctor_def
: id           { ($1, None)    }
| id KW_OF typ { ($1, Some $3) }
;

ctor_def_bar_sep_list_rev
: ctor_def { [ $1 ] }
| ctor_def_bar_sep_list_rev BAR ctor_def { $3 :: $1 }
;

/* ========================================================================= */

bar_opt
: /* empty */ { () }
| BAR         { () }
;

/* ========================================================================= */

id
: ID { mk_ident $1 }
;

id_list
: id_list_rev { List.rev $1 }
;

id_list_rev
: /* empty */ { [] }
| id_list_rev id { $2 :: $1 }
;

tvar_id
: TVAR { mk_ident $1 }
;

tvar_id_list_rev
: /* empty */ { [] }
| tvar_id_list_rev tvar_id { $2 :: $1 }
;

tvar_id_comma_sep_list_rev
: tvar_id { [ $1 ] }
| tvar_id_comma_sep_list_rev COMMA tvar_id { $3 :: $1 }
;
