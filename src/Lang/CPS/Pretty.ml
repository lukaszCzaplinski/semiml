open Common.SmlConst

module Box = Printing.Box

module Env = Printing.Scope.Make(Common.Var)

let pretty_primop op =
  Box.text ~attributes: [Box.Constant]
    begin match op with
    | Primop.Arith op  -> Common.Primop.Arith.alpha_name op
    | Primop.FArith op -> Common.Primop.FArith.alpha_name op
    | Primop.Mem op    -> Common.Primop.Mem.alpha_name op
    | Primop.Repr op   -> Common.Primop.Repr.alpha_name op
    | Primop.Exn op    -> Common.Primop.Exn.alpha_name op
    end

let rec pretty_formal_args_aux env xs =
  match xs with
  | [] -> assert false
  | [x] ->
    let (env, x) = Env.extend env x in
    (env, [ Box.prefix (Box.oper ",")
        (Box.suffix x (Box.text ~attributes:[Box.Paren] ")")) ])
  | x :: xs ->
    let (env, x) = Env.extend env x in
    let (env, xs) = pretty_formal_args_aux env xs in
    (env, Box.prefix (Box.oper ",") x :: xs)

let pretty_formal_args env xs =
  match xs with
  | [] -> (env, Box.text ~attributes:[Box.Paren] "()")
  | [x] ->
    let (env, x) = Env.extend env x in
    (env, Box.paren x)
  | x :: xs ->
    let (env, x) = Env.extend env x in
    let (env, xs) = pretty_formal_args_aux env xs in
    (env, Box.box (Box.prefix (Box.text ~attributes:[Box.Paren] "(") x :: xs))

let rec pretty_access_path ap =
  match ap with
  | Ast.Offp 0 -> []
  | Ast.Offp n ->
    [ Box.box
      [ Box.white_sep (Box.keyword "offset")
      ; Box.white_sep 
          (Box.text ~attributes:[Box.Number] (sml_string_of_int n))
      ]
    ]
  | Ast.Selp(n, ap) ->
    Box.box
    [ Box.oper "#"
    ; Box.text ~attributes:[Box.Number] (sml_string_of_int n)
    ] :: pretty_access_path ap

let pretty_value env v =
  match v with
  | Ast.Var x   -> Env.pretty env x
  | Ast.Label x -> 
    Box.box [ Box.keyword "label"; Box.white_sep (Env.pretty env x) ]
  | Ast.Int n ->
    Box.text ~attributes:[Box.Number] (sml_string_of_int64 n)
  | Ast.Real r ->
    Box.text ~attributes:[Box.Number] (Common.Real.to_sml_string r)
  | Ast.String s ->
    Box.text ~attributes:[Box.Literal] (sml_string_of_string s)

let pretty_record_field env (v, ap) =
  Box.indent 2 (Box.box (pretty_value env v :: pretty_access_path ap))
  |> Box.br

let rec pretty_args_aux env vs =
  match vs with
  | [] -> assert false
  | [v] ->
    [ Box.prefix (Box.oper ",")
        (Box.suffix (pretty_value env v)
          (Box.text ~attributes:[Box.Paren] ")")) ]
  | v :: vs ->
    Box.prefix (Box.oper ",") (pretty_value env v)
    :: pretty_args_aux env vs

let pretty_args env vs =
  match vs with
  | [] -> Box.text ~attributes:[Box.Paren] "()"
  | [v] -> Box.paren (pretty_value env v)
  | v :: vs ->
    Box.box
    (  Box.prefix (Box.text ~attributes:[Box.Paren] "(") (pretty_value env v)
    :: pretty_args_aux env vs)

let rec pretty_expr env expr =
  match expr.Ast.e_kind with
  | Ast.Record(vaps, x, expr) ->
    let (env', x) = Env.extend env x in
    let ins =
      Box.box
      (( Box.box
         [ Box.keyword "let"
         ; Box.white_sep x
         ; Box.white_sep (Box.oper "=")
         ; Box.white_sep (Box.keyword "record")
         ] |> Box.br)
      :: List.map (pretty_record_field env) vaps)
    in ins :: pretty_expr env' expr
  | Ast.Select(n, v, x, expr) ->
    let (env', x) = Env.extend env x in
    let ins =
      Box.box
      [ Box.keyword "let"
      ; Box.white_sep x
      ; Box.white_sep (Box.oper "=")
      ; Box.white_sep (Box.box
        [ Box.oper "#"
        ; Box.text ~attributes:[Box.Number] (sml_string_of_int n)
        ])
      ] |> Box.br
    in ins :: pretty_expr env' expr
  | Ast.Offset(n, v, x, expr) ->
    let (env', x) = Env.extend env x in
    let ins =
      Box.box
      [ Box.keyword "let"
      ; Box.white_sep x
      ; Box.white_sep (Box.oper "=")
      ; Box.white_sep (Box.box
        [ Box.keyword "offset"
        ; Box.white_sep 
            (Box.text ~attributes:[Box.Number] (sml_string_of_int n))
        ])
      ] |> Box.br
    in ins :: pretty_expr env' expr
  | Ast.App(v, vs) ->
    [ Box.box
      [ Box.keyword "call"
      ; Box.white_sep (pretty_value env v)
      ; Box.indent 2 (pretty_args env vs)
      ] |> Box.br
    ]
  | Ast.Fix(defs, expr) ->
    let (env, defs) = create_fix_env env defs in
    pretty_fix_expr env defs expr
  | Ast.Switch(v, clauses) ->
    [ Box.box
      (( Box.box
         [ Box.keyword "switch"
         ; Box.white_sep (pretty_value env v)
         ] |> Box.br)
      :: List.map (fun expr ->
          Box.prefix (Box.oper "=>") (Box.box (pretty_expr env expr))
        ) clauses
      @ [Box.keyword "end" |> Box.br])
    ]
  | Ast.Primop(op, args, [], conts) ->
    ( Box.box
      [ Box.keyword "primop"
      ; Box.white_sep (pretty_primop op)
      ; pretty_args env args
      ] |> Box.br)
    :: pretty_primop_cont env conts
  | Ast.Primop(op, args, [x], conts) ->
    let (env', x) = Env.extend env x in
    ( Box.box
      [ Box.keyword "let"
      ; Box.white_sep x
      ; Box.white_sep (Box.oper "=")
      ; Box.white_sep (Box.keyword "primop")
      ; Box.white_sep (pretty_primop op)
      ; pretty_args env args
      ] |> Box.br)
    :: pretty_primop_cont env' conts
  | Ast.Primop(op, args, vars, conts) ->
    let (env', vars) = pretty_primop_vars env vars in
    ( Box.box
      [ Box.keyword "primop"
      ; Box.white_sep (pretty_primop op)
      ; pretty_args env args
      ; Box.white_sep (Box.keyword "as")
      ; Box.white_sep (Box.box vars)
      ] |> Box.br)
    :: pretty_primop_cont env' conts

and create_fix_env env defs =
  match defs with
  | [] -> (env, [])
  | (x, args, body) :: defs ->
    let (env, x) = Env.extend env x in
    let (env, defs) = create_fix_env env defs in
    (env, (x, args, body) :: defs)

and pretty_fix_aux env defs expr =
  match defs with
  | [] -> pretty_expr env expr
  | (x, args, body) :: defs ->
    let (body_env, args) = pretty_formal_args env args in
    Box.box
    [ (Box.box
      [ Box.keyword "and"
      ; Box.white_sep x
      ; args
      ; Box.white_sep (Box.oper "=")
      ] |> Box.br)
    ; Box.indent 2 (Box.box (pretty_expr body_env body))
    ] :: pretty_fix_aux env defs expr

and pretty_fix_expr env defs expr =
  match defs with
  | [] -> pretty_expr env expr
  | (x, args, body) :: defs ->
    let (body_env, args) = pretty_formal_args env args in
    Box.box
    [ (Box.box
      [ Box.keyword "fix"
      ; Box.white_sep x
      ; args
      ; Box.white_sep (Box.oper "=")
      ] |> Box.br)
    ; Box.indent 2 (Box.box (pretty_expr body_env body))
    ] :: pretty_fix_aux env defs expr

and pretty_primop_vars env xs =
  match xs with
  | [] -> assert false
  | [x] ->
    let (env, x) = Env.extend env x in
    (env, [x])
  | x :: xs ->
    let (env, x) = Env.extend env x in
    let (env, xs) = pretty_primop_vars env xs in
    (env, Box.suffix x (Box.oper ",") :: xs)

and pretty_primop_cont env conts =
  match conts with
  | [e] -> pretty_expr env e
  | _ ->
    [ Box.box
      ( List.map (fun expr ->
          Box.prefix (Box.oper "=>") (Box.box (pretty_expr env expr))
        ) conts
      @ [ Box.keyword "end" |> Box.br ])
    ]

let pretty_program expr =
  let env = Env.create Common.Var.name in
  let (env, _) = Env.extend env Ast.top_cont in
  Box.box (pretty_expr env expr)
