
type t =
| Arith   of Common.Primop.Arith.t
| FArith  of Common.Primop.FArith.t
| Mem     of Common.Primop.Mem.t
| Repr    of Common.Primop.Repr.t
| Exn     of Common.Primop.Exn.t
