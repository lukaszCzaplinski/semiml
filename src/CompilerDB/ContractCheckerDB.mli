
val register_contract_checker : ContractChecker.t -> unit

val possible_checkers : 
  'lang Types.language -> Contract.t -> ContractChecker.t list
