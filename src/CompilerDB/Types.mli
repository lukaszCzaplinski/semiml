
type _ language =
| Lang_CPS       : Lang.CPS.Ast.expr language
| Lang_Lambda    : Lang.Lambda.Ast.expr language
| Lang_MiniML    : Lang.MiniML.Ast.expr language
| Lang_NuL       : Lang.NuL.Ast.expr language
| Lang_RawCPS    : Lang.RawCPS.Ast.expr language
| Lang_RawMiniML : Lang.RawMiniML.Ast.expr language
| Lang_RawNuL    : Lang.RawNuL.Ast.expr language

type 'lang analysis =
  'lang -> unit

type 'lang contract_checker =
  'lang -> bool

type 'lang evaluator =
  'lang -> unit

type 'lang parser_func = 
  Lexing.lexbuf -> 'lang

type 'lang pretty_printer =
  'lang -> Printing.Box.t

type ('source, 'target) transformation =
  'source -> 'target

type state =
| State : 'lang language * 'lang * Contract.Set.t -> state

module Language : sig
  type 'lang t = 'lang language
  val compare      : 'a t -> 'b t -> int
  val equal        : 'a t -> 'b t -> bool
  val get_equality : 'a t -> 'b t -> ('a, 'b) Equality.t
end
