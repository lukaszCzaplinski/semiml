
type _ language =
| Lang_CPS       : Lang.CPS.Ast.expr language
| Lang_Lambda    : Lang.Lambda.Ast.expr language
| Lang_MiniML    : Lang.MiniML.Ast.expr language
| Lang_NuL       : Lang.NuL.Ast.expr language
| Lang_RawCPS    : Lang.RawCPS.Ast.expr language
| Lang_RawMiniML : Lang.RawMiniML.Ast.expr language
| Lang_RawNuL    : Lang.RawNuL.Ast.expr language

type 'lang analysis =
  'lang -> unit

type 'lang contract_checker =
  'lang -> bool

type 'lang evaluator =
  'lang -> unit

type 'lang parser_func = 
  Lexing.lexbuf -> 'lang

type 'lang pretty_printer =
  'lang -> Printing.Box.t

type ('source, 'target) transformation =
  'source -> 'target

type state =
| State : 'lang language * 'lang * Contract.Set.t -> state

module Language = struct
  type 'a t = 'a language

  let order (type a) (type b) (l1 : a t) (l2 : b t)
      : (a, b) Equality.ord =
    match l1, l2 with
    | Lang_CPS,       Lang_CPS       -> Equality.Ord_Eq
    | Lang_CPS,       _              -> Equality.Ord_Lt
    | _,              Lang_CPS       -> Equality.Ord_Gt
    | Lang_Lambda,    Lang_Lambda    -> Equality.Ord_Eq
    | Lang_Lambda,    _              -> Equality.Ord_Lt
    | _,              Lang_Lambda    -> Equality.Ord_Gt
    | Lang_MiniML,    Lang_MiniML    -> Equality.Ord_Eq
    | Lang_MiniML,    _              -> Equality.Ord_Lt
    | _,              Lang_MiniML    -> Equality.Ord_Gt
    | Lang_NuL,       Lang_NuL       -> Equality.Ord_Eq
    | Lang_NuL,       _              -> Equality.Ord_Lt
    | _,              Lang_NuL       -> Equality.Ord_Gt
    | Lang_RawCPS,    Lang_RawCPS    -> Equality.Ord_Eq
    | Lang_RawCPS,    _              -> Equality.Ord_Lt
    | _,              Lang_RawCPS    -> Equality.Ord_Gt
    | Lang_RawMiniML, Lang_RawMiniML -> Equality.Ord_Eq
    | Lang_RawMiniML, _              -> Equality.Ord_Lt
    | _,              Lang_RawMiniML -> Equality.Ord_Gt
    | Lang_RawNuL,    Lang_RawNuL    -> Equality.Ord_Eq

  let compare (type a) (type b) (l1 : a t) (l2 : b t) =
    match order l1 l2 with
    | Equality.Ord_Lt -> -1
    | Equality.Ord_Eq -> 0
    | Equality.Ord_Gt -> 1

  let equal l1 l2 = compare l1 l2 = 0
  
  let get_equality (type a) (type b) (l1 : a t) (l2 : b t) 
      : (a, b) Equality.t =
    match order l1 l2 with
    | Equality.Ord_Eq -> Equality.Eq
    | _               -> Equality.Neq
end
