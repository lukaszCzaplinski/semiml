open Lang.MiniML.Ast
module L = Lang.Lambda.Ast

type case_int_expr =  Lang.MiniML.Ast.expr * (int64 * Lang.MiniML.Ast.expr) list * Lang.MiniML.Ast.default_case
type case_exn_expr = Lang.MiniML.Ast.expr * (Lang.MiniML.Ast.expr * Lang.MiniML.Ast.var option * Lang.MiniML.Ast.expr) list * Lang.MiniML.Ast.default_case
type con_type = Lang.MiniML.Type.cname * Lang.MiniML.Type.t option
type con_numbered_type = int * Lang.MiniML.Type.t option

let (<|) f x = f x
let (<~) f g x = f(g(x))

let nyi str = failwith <| "ToLambda::NotYetImplemented::" ^ str

let has_some = function
  | None -> false
  | _ -> true

type proto_func_app = { func_var : Lang.Lambda.Ast.var;
                        func_body : Lang.Lambda.Ast.expr;
                        arg : Lang.Lambda.Ast.expr }

let cast_primop : Lang.MiniML.Primop.t -> Lang.Lambda.Primop.t = function
  | Lang.MiniML.Primop.Arith p -> Lang.Lambda.Primop.Arith p
  | Lang.MiniML.Primop.FArith p -> Lang.Lambda.Primop.FArith p
  | Lang.MiniML.Primop.Mem p -> Lang.Lambda.Primop.Mem p
  | Lang.MiniML.Primop.Repr p -> Lang.Lambda.Primop.Repr p
  | Lang.MiniML.Primop.Control p -> Lang.Lambda.Primop.Control p

let rec transform : Lang.MiniML.Ast.expr -> Lang.Lambda.Ast.expr =
  fun { e_tag; e_kind } ->
    let wrap_ekind : Lang.Lambda.Ast.expr_kind -> Lang.Lambda.Ast.expr =
      fun ekind -> { L.e_tag = e_tag; L.e_kind = ekind }

    in let create_func_app : proto_func_app -> Lang.Lambda.Ast.expr = fun proto ->
        wrap_ekind <| L.App(wrap_ekind <| L.Fn(proto.func_var, proto.func_body), proto.arg)

    in let cons : unit -> con_type list = fun () ->
        let info = MetaData.MiniML.Datatypes.get e_tag in
        let open MetaData.MiniML.Datatypes in
        let open Lang.MiniML.Type in info.datatype.dt_ctors

    in let get_cons_by_type : unit -> (con_type list) * (con_type list) = fun () ->
         List.partition (fun (_, opt) -> not <| has_some opt) (cons ())

    in let get_con_num cname =
         let find_some_index l = match List.concat <| List.mapi (fun i (c,_) -> if c == cname then [i] else []) l with
           | [i] -> Some i
           | _ -> None in
         let (constant_cons, arg_cons) = get_cons_by_type() in
         match find_some_index constant_cons with
         | Some i -> i
         | None -> (match find_some_index arg_cons with
              | Some i -> i
              | None -> assert false)

    in let get_cons_by_typei : unit -> con_numbered_type list * con_numbered_type list = fun () ->
        let add_num = List.map (fun (cname, cargs) -> (get_con_num cname, cargs)) in
        let (c1, c2) = get_cons_by_type() in
        (add_num c1, add_num c2)

    in let constant_cons_num : unit -> int = fun () -> let (constant_cons, _) = get_cons_by_type() in List.length <| constant_cons

    in let is_sole_cons : unit -> bool = fun () -> 1 = (List.length <| cons ())

    in let transform_fix_def : Lang.MiniML.Ast.fix_def -> Lang.Lambda.Ast.var * Lang.Lambda.Ast.expr =
         fun { fxd_var; fxd_type; fxd_body } -> (fxd_var, transform fxd_body)

    in let transform_case_int : case_int_expr -> L.expr = fun (e, es, (v, ed)) ->
        (* NOTE: this returns expression in form LET fresh_var = e IN SWITCH v [C1.., CN] DEFAULT: ed *)
        let fresh_var = Common.Var.create () in
        let switch_expr = {
          L.sw_expr           = wrap_ekind <| L.Var fresh_var
        ; L.sw_possible_cons  = []
        ; L.sw_con_cases      = []
        ; L.sw_constant_range = None (* Some (Int64.of_int (List.fold_left max 0 (List.map (fun (v,_) -> Int64.to_int v) es))) *)
        ; L.sw_constant_cases = List.map (fun (va, en) -> (va, transform en)) es
        ; L.sw_default_case   = Some( create_func_app { func_var = v;
                                                        func_body = transform ed;
                                                        arg = wrap_ekind <| L.Var(fresh_var) } )
        } in create_func_app { func_var = fresh_var;
                               func_body = wrap_ekind <| L.Switch(switch_expr);
                               arg = transform e }

    in let transform_con : Lang.MiniML.Type.cname * Lang.MiniML.Ast.expr option -> L.expr = fun (cname, eo) ->
        let con_num = get_con_num cname in
        wrap_ekind <| match eo with
        | None -> L.Int(Int64.of_int con_num)
        | Some e -> if is_sole_cons()
          then L.Con(L.C_Transparent, transform e)
          else L.Con(L.C_Tagged(con_num), transform e)

    in let transform_case : Lang.MiniML.Ast.case_expr -> Lang.Lambda.Ast.expr =
         fun { ce_expr; ce_cases; ce_default_case } ->
           let (constant_cons, arg_cons) = get_cons_by_typei() in

           let possible_cons = if is_sole_cons()
             then [L.C_Transparent]
             else List.map (fun (i, _) -> L.C_Tagged i) arg_cons  in

           let get_con  =
             let cons = cons() in
             fun cname ->
               let (_, arg_opt) = List.find (fun (name, _) -> name = cname) cons
               in arg_opt in

           let (con_cases_raw, constant_cases_raw) =
             List.partition (fun (cname, _, _) -> has_some <| get_con cname) ce_cases in

           let fresh_var = Common.Var.create () in
           let wrap_func con vo e = match vo with
             | None -> (con, transform e)
             | Some v -> (con, create_func_app { func_var = v;
                                                 func_body = transform e;
                                                 arg = wrap_ekind <| L.Decon(con, wrap_ekind <| L.Var(fresh_var)) } ) in

           let constant_cases =
             List.map (fun (cname, vo, e) -> (Int64.of_int <| get_con_num cname, transform e)) constant_cases_raw in

           let con_cases =
             List.map (fun (cname, vo, e) -> wrap_func (L.C_Tagged(get_con_num cname)) vo e) con_cases_raw in

           let default_case = match ce_default_case with
             | None  -> None
             | Some (v,e) -> Some(create_func_app { func_var = v;
                                                    func_body = transform e;
                                                    arg = wrap_ekind( L.Var(fresh_var))} ) in

           let constant_range = let num = constant_cons_num() in if num = 0 then None else Some (Int64.of_int num) in

           let switch_expr = {
             L.sw_expr           = wrap_ekind <| L.Var fresh_var
           ; L.sw_possible_cons  = possible_cons
           ; L.sw_con_cases      = con_cases
           ; L.sw_constant_range = constant_range
           ; L.sw_constant_cases = constant_cases
           ; L.sw_default_case   = default_case
           } in create_func_app { func_var = fresh_var;
                                  func_body = wrap_ekind <| L.Switch(switch_expr);
                                  arg = transform ce_expr }

    in let transform_exn_ctor : Lang.MiniML.Ast.expr -> Lang.Lambda.Ast.expr = fun e ->
        wrap_ekind <| L.App(wrap_ekind <| L.Prim(Lang.Lambda.Primop.Mem Common.Primop.Mem.MakeRef), transform e)

    in let transform_case_exn : case_exn_expr-> Lang.Lambda.Ast.expr = fun (exc, es, (v, e)) ->
        let fresh_var = Common.Var.create() in
        let fresh_var_value = wrap_ekind <| L.Var(fresh_var) in
        let transform_pattern_rule (n, vo, e) = match vo with
          | None -> (wrap_ekind <| L.ExnName(transform n), transform e)
          | Some v -> (transform n, create_func_app { func_var = v;
                                                      func_body = transform e;
                                                      arg = wrap_ekind <| L.ExnValue(fresh_var_value)}) in
        let switch_body = List.map transform_pattern_rule es in
        let switch_default = create_func_app { func_var = v;
                                               func_body = transform e;
                                               arg = fresh_var_value } in
        create_func_app { func_var = fresh_var;
                          func_body = wrap_ekind <| L.SwitchExn(fresh_var_value, switch_body, switch_default);
                          arg = transform exc }

    in match e_kind with
    | TypeDef(_, _, _, e) -> transform e
    | DataType(_, e) -> transform e
    | TypeVarFn(_, e) -> transform e
    | TypeVarApp(e, _) -> transform e
    | TypeFn(_, _, e) -> transform e
    | TypeApp(e, _, _) -> transform e
    | Pack(_, _, _, e, _) -> transform e
    | Unpack(_, _, x, e1, e2) -> wrap_ekind <|  L.App( wrap_ekind <|  L.Fn(x, transform e2) , transform e1)
    | Var(v) -> wrap_ekind <| L.Var(v)
    | Fn(v, _, e) -> wrap_ekind <| L.Fn(v, transform e)
    | Fix(fdef, e) -> wrap_ekind <| L.Fix(List.map transform_fix_def fdef, transform e)
    | App(f, v) -> wrap_ekind <| L.App(transform f, transform v)
    | Int(v) -> wrap_ekind <| L.Int(v)
    | Real(v) -> wrap_ekind <| L.Real(v)
    | String(v) -> wrap_ekind <| L.String(v)
    | Record(es) -> wrap_ekind <| L.Record(List.map transform es)
    | Select(v, e) -> wrap_ekind <| L.Select(v, transform e)
    | CaseInt(e, es, ed) -> transform_case_int(e, es, ed)
    | Prim(p) -> wrap_ekind <| L.Prim(cast_primop p)
    | Con(cname, _, eo) -> transform_con(cname, eo)
    | Case(ce) -> transform_case(ce)
    | Raise(e, _) -> wrap_ekind <| L.Raise(transform e)
    | Handle(e1, e2) -> wrap_ekind <| L.Handle(transform e1, transform e2)
    | Exn(e) -> wrap_ekind <| L.ConExn(wrap_ekind <| L.Int(0L), wrap_ekind <| L.App(wrap_ekind <| L.Prim(Lang.Lambda.Primop.Mem Common.Primop.Mem.MakeRef), transform e))
    | ExnCtor(e, _) -> transform_exn_ctor(e)
    | ConExn(e1, e2) -> wrap_ekind <| L.ConExn(transform e2, transform e1)
    | ExnName(e) -> wrap_ekind <| L.ExnName(transform e)
    | CaseExn(e, es, ed) -> transform_case_exn(e, es, ed)


let c_to_lambda = Contract.create "transform:to_lambda"

let register () =
  Compiler.register_transformation
    ~source: Compiler.Lang_MiniML
    ~target: Compiler.Lang_Lambda
    ~name:   "MiniML:to_Lambda"
    ~require:
      [ Lang.MiniML.Contracts.well_typed
      (* ; Lang.MiniML.Contracts.unique_vars WOULD BE NICE *)
      ]
    ~contracts:
      [ c_to_lambda
      ]
    transform
