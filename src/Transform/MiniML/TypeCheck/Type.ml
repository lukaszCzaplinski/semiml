
module S  = Lang.RawMiniML.Ast
module TT = Lang.MiniML.Type

let rec add_tvars env xs =
  match xs with
  | [] -> (env, [])
  | x :: xs ->
    let (env, x)  = Env.add_tvar env x.S.id_name in
    let (env, xs) = add_tvars env xs in
    (env, x :: xs)

let check_var env tag x =
  try 
    let x = Env.get_tvar env x in
    TT.Var x
  with
  | Not_found ->
    Errors.error ~tag: tag
      "Unbound type variable %s." x;
    raise Errors.Fatal_error

let rec check_tcon env tag args x =
  try
    let y = Env.get_type env x.S.id_name in
    if List.length args = Env.tcon_arity env y then
      TT.TyCon(List.map (check env) args, y)
    else begin
      Errors.error ~tag: tag
        "Type %s expects %d argument(s), \
        but here is applied to %d argument(s)."
        x.S.id_name
        (Env.tcon_arity env y)
        (List.length args);
      raise Errors.Fatal_error
    end
  with
  | Not_found ->
    Errors.error ~tag: x.S.id_tag
      "Unbound type %s." x.S.id_name;
    raise Errors.Fatal_error

and check_typedef env tdef =
  let (env, args) = add_tvars env tdef.S.tdef_args in
  let body = check env tdef.S.tdef_body in
  (args, body)

and check_local_typedefs env defs tp =
  match defs with
  | [] -> check env tp
  | def :: defs ->
    let (args, body) = check_typedef env def in
    let (env, x) = Env.add_typedef env def.S.tdef_name.S.id_name args body in
    TT.TypeDef(args, x, body, check_local_typedefs env defs tp)

and build_datatypes_env env defs =
  match defs with
  | [] -> (env, [])
  | def :: defs ->
    let (env, x) = 
      Env.add_abstype env def.S.dt_name.S.id_name (List.length def.S.dt_args)
    in
    let (env, defs) = build_datatypes_env env defs in
    (env, (x, def.S.dt_args, def.S.dt_ctors) :: defs)

and check_datatype_ctors env args x body_env ctors =
  match ctors with
  | [] -> (env, [])
  | (ctor, None) :: ctors ->
    let (env, ctor) = 
      Env.add_const_ctor env ctor.S.id_name (List.length args) x in
    let (env, ctors) = check_datatype_ctors env args x body_env ctors in
    (env, (ctor, None) :: ctors)
  | (ctor, Some tp) :: ctors ->
    let tp = check body_env tp in
    let (env, ctor) =
      Env.add_data_ctor env ctor.S.id_name args x tp in
    let (env, ctors) = check_datatype_ctors env args x body_env ctors in
    (env, (ctor, Some tp) :: ctors)

and check_tmp_datatypes env defs =
  match defs with
  | [] -> (env, [])
  | (x, args, ctors) :: defs ->
    let (body_env, args) = add_tvars env args in
    let (env, ctors) = check_datatype_ctors env args x body_env ctors in
    let env = Env.add_datatype env args x ctors in
    let (env, defs) = check_tmp_datatypes env defs in
    (env, { TT.dt_name = x; TT.dt_args = args; TT.dt_ctors = ctors } :: defs)

and check_datatypes env defs =
  let (env, tmpdefs) = build_datatypes_env env defs in
  check_tmp_datatypes env tmpdefs

and check_local_datatypes env defs tp =
  let (env, defs) = check_datatypes env defs in
  TT.DataType(defs, check env tp)

and check_forall_var env xs body =
  match xs with
  | []      -> check env body
  | x :: xs ->
    let (env, x) = Env.add_tvar env x.S.id_name in
    TT.ForallVar(x, check_forall_var env xs body)

and check_forall env decls body =
  match decls with
  | [] -> check env body
  | decl :: decls ->
    let n = List.length decl.S.tdecl_args in
    let (env, x) = Env.add_abstype env decl.S.tdecl_name.S.id_name n in
    TT.Forall(n, x, check_forall env decls body)

and check_exists env decls body =
  match decls with
  | [] -> check env body
  | decl :: decls ->
    let n = List.length decl.S.tdecl_args in
    let (env, x) = Env.add_abstype env decl.S.tdecl_name.S.id_name n in
    TT.Exists(n, x, check_exists env decls body)

and check env tp =
  match tp.S.t_kind with
  | S.T_Var x -> check_var env tp.S.t_tag x
  | S.T_Con(args, x) -> check_tcon env tp.S.t_tag args x
  | S.T_Arrow(tp1, tp2) -> TT.Arrow(check env tp1, check env tp2)
  | S.T_Record tps -> TT.Record(List.map (check env) tps)
  | S.T_TypeDef(defs, tp) -> check_local_typedefs env defs tp
  | S.T_Datatype(defs, tp) -> check_local_datatypes env defs tp
  | S.T_ForallVar(xs, body) -> check_forall_var env xs body
  | S.T_Forall(decls, body) -> check_forall env decls body
  | S.T_Exists(decls, body) -> check_exists env decls body
